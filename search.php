<?php get_header(); ?>
<?php get_template_part( 'template-parts/page-search', 'form' ); ?>
<section class="content_posts archive_page">
  <div class="row archive_content">
    <div class="col archive_content_main">
    <?php if ( have_posts() ) : ?>
      <div class="row archive_page_list">
        <?php while ( have_posts() ) : the_post(); ?>
        <?php get_template_part( 'template-parts/archive', 'loop' ); ?>
        <?php endwhile; ?>     
      </div> <!-- .content_posts_list -->
        <?php get_template_part('template-parts/pagination'); ?>
        <?php else : ?>
      <div class="row archive_page_list">
        <div class="col archive_page_list_404">
          <h2 class="noresults">No results found for "<?php echo get_search_query(); ?>"</h2>   
        </div>          
      </div> <!-- .content_posts_list -->
    <?php endif; ?> 
    <?php wp_reset_postdata(); ?>
    </div> <!-- .archive_content_main -->   

    <div class="col archive_content_sidebar">
      <?php if ( is_active_sidebar( 'sidebar-one' ) ) : ?>
        <?php dynamic_sidebar( 'sidebar-one' ); ?>
      <?php endif; ?>
    </div> <!-- .archive_content_sidebar -->
  </div> <!-- .archive_content -->
</section>   
<?php get_footer(); ?>