<?php get_header(); ?>
<?php echo $categoryId; ?>
<section class="content_wrapper" id="content-single">
 <div class="row content_main">
  <main class="post" id="post-content" role="main">
      <?php if ( have_posts() ): ?>
      <?php while ( have_posts() ): the_post(); ?>
      <?php the_title( '<h1 class="post_title content_title">', '</h1>' ); ?>
      <article class="post_content">
        <?php the_content(); ?>
        <div>&nbsp;</div>
        <div class="post_share post_share-bottom">
          <span class="post_share_label">SHARE THIS PAGE:&nbsp; </span> <?php get_template_part( 'inc/social', 'share' ); ?>
        </div>
        <?php if ( is_active_sidebar( 'sidebar-two' ) ) : ?>
          <?php dynamic_sidebar( 'sidebar-two' ); ?>
        <?php endif; ?>
      </article>    
      <?php endwhile; ?> 
      <?php else: ?>
          <p>No content available.</p>
      <?php endif; ?>
    </main>
 </div>
</section>
<?php get_footer(); ?>