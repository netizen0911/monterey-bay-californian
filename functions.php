<?php

add_theme_support( 'menus' );
add_theme_support( 'post-thumbnails' );

//Add 1st Sidebar
add_action( 'widgets_init', 'sidebarone' );
function sidebarone() {
  register_sidebar(
      array (
          'name' => __( 'Sidebar One', 'Monterey Bay Californian' ),
          'id' => 'sidebar-one',
          'description' => __( 'This is the main Sidebar', 'Monterey Bay Californian' ),
          'before_widget' => '<div class="widget-content">',
          'after_widget' => "</div>",
          'before_title' => '<h3 class="widget-title">',
          'after_title' => '</h3>',
      )
  );
}

//Add 2nd Sidebar
add_action( 'widgets_init', 'sidebartwo' );
function sidebartwo() {
  register_sidebar(
      array (
          'name' => __( 'Sidebar Two', 'Monterey Bay Californian' ),
          'id' => 'sidebar-two',
          'description' => __( 'This appears every after single post', 'Monterey Bay Californian' ),
          'before_widget' => '<div class="widget-content">',
          'after_widget' => "</div>"
      )
  );
}


// Add Pre-footer Menu
add_action( 'init', 'register_my_menus' );
function register_my_menus() {
  register_nav_menus(
    array(
      'primary-menu' => __( 'Primary Menu', 'text_domain' ),
      'footer-menu' => __( 'Footer Menu' ),
      'sidebar-menu' => __( 'Sidebar Menu' )
    )
  );
}


add_action( 'wp_enqueue_scripts', 'mbc_theme_scripts' );
function mbc_theme_scripts() {
  wp_enqueue_style( 'main-style', get_stylesheet_uri() );

  wp_dequeue_script('jquery');
  wp_dequeue_script('jquery-core');
  wp_dequeue_script('jquery-migrate');
  wp_enqueue_script('jquery', false, array(), false, true);
  wp_enqueue_script('jquery-core', false, array(), false, true);
  wp_enqueue_script('jquery-migrate', false, array(), false, true);
  wp_enqueue_script( 'main-script', get_template_directory_uri() . '/assets/js/scripts.js', array(), '1.0.0', true );
}

// Embed load-more script into footer
require get_template_directory().'/inc/loadmore-ajax.php';

// Add show more posts pagination
require get_template_directory().'/inc/loadmore-function.php';

// Limit search results per page
add_filter('request', 'change_wp_search_size'); // Hook our custom function onto the request filter
function change_wp_search_size($queryVars) {
  if ( isset($_REQUEST['s']) ) // Make sure it is a search page
      $queryVars['posts_per_page'] = 5; // Change 10 to the number of posts you would like to show
  return $queryVars; // Return our modified query variables
}

// Limit Category Entries
add_filter('pre_get_posts', 'limit_category_posts');
function limit_category_posts($query){
    if(is_category()) {
      if ($query->is_category) {
        $query->set('posts_per_page', 5);
      }
      return $query;
    }
}

//Limit excerpt length
add_filter( 'excerpt_length', 'tn_custom_excerpt_length', 999 );
function tn_custom_excerpt_length( $length ) {
  return 15;
  }