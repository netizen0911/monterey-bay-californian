<?php

// Load More for Blog

add_action('wp_ajax_load_posts_by_ajax', 'load_posts_by_ajax_callback');
add_action('wp_ajax_nopriv_load_posts_by_ajax', 'load_posts_by_ajax_callback');

function load_posts_by_ajax_callback() {
	check_ajax_referer('load_more_posts', 'security');
	$paged = $_POST['page'];
	$args = array(
    'post_type' => 'post',
    'post_status' => 'publish',
    'posts_per_page' => '3',
    'paged' => $paged + 1,
	);
	$the_query = new WP_Query( $args );
	if ( $the_query->have_posts() ) :
			?>
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
			<?php get_template_part( 'template-parts/posts', 'loop' ); ?>
      <?php endwhile; ?>
      
      <?php
      
	endif;

	wp_die();
}