<?php

//Load more for Blog posts
add_action( 'wp_footer', 'embed_loadmore_script', 100 );
function embed_loadmore_script() { 	
	if(is_blog()) {
		?>
		<script>
			var ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
			var page = 2;
			jQuery(function($) {
					$('body').on('click', '.loadmore', function() {
							var data = {
								'action': 'load_posts_by_ajax',
								'page': page,
								'security': '<?php echo wp_create_nonce("load_more_posts"); ?>',
								'max_page': '<?php echo $the_query->max_num_pages + 1; ?>'
							};	

							$('.loadmore').hide();
							$('.show-loader').show();				
			
							$.post(ajaxurl, data, function(response) {
	
									if(response != '') {
										$('.content_post_list').append(response);
										page++;

										$('.loadmore').show();
										$('.show-loader').hide();

									} else {
										$('.loadmore').hide();
										$('.show-loader').hide();
										$('.show-more').html('<p>No more posts...</p>')
									}
									
							});        
					});
			});
		</script>
	<?php
	}	
}

// If it's blog or archive page
function is_blog() {
	return ( 
		// is_author() || 
		// is_category() || 
    is_single() || 
    is_search() ||
	is_archive() || 
	is_home() || 
	is_tag()) && 'post' == get_post_type();
}