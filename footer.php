<section class="pre-footer">
      <div class="row">
        <div class="col">
        <?php
          wp_nav_menu( array( 
            'theme_location' => 'footer-menu', 
            'menu_class' => 'footer_menu_list',
            'container_class' => 'footer_menu' )            
          ); 
        ?>
        </div>
        <div class="col">
          <div class="pre-footer_social">
            <span><a href=""><i class="fa fa-facebook-f"></i></a></span>
            <span><a href=""><i class="fa fa-twitter"></i></a></span>
            <span><a href=""><i class="fa fa-instagram"></i></a></span>
            <span><a href=""><i class="fa fa-pinterest-p"></i></a></span>
          </div>
        </div>
      </div>          
    </section>

    <footer class="footer">
      <div class="row">
          <div class="col footer_logo">
            <a href="<?php echo home_url(); ?>">
              <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/assets/img/logo-footer.png" alt="">
            </a>
          </div>
          <div class="col footer_tos">
            <a href="">TERMS OF USE</a>
            <a href="">PRIVACY POLICY</a>
          </div>
          <div class="col footer_copy">
            <p>&copy; 2020. MONTEREY BAY CALIFORNIAN. ALLRIGHTS RESERVED.</p>
          </div>
      </div>
    </footer>
    <div id="overlay"></div>
    <?php wp_footer(); ?>
    <a href="javascript:void(0);" id="toTop" title="Go to top"><i class="fa fa-angle-up"></i></a>
    <script src="<?php echo get_bloginfo( 'template_directory' ); ?>/assets/js/scripts.js"></script>
    <script src="<?php echo get_bloginfo( 'template_directory' ); ?>/assets/js/mobile-nav.js"></script>
  </body>
</html>