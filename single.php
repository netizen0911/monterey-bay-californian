<?php get_header(); ?>
<?php echo $categoryId; ?>
<section class="content_wrapper" id="content-single">
 <div class="row content_main">
  <main class="post" id="post-content" role="main">
      <?php if ( have_posts() ): ?>
      <?php while ( have_posts() ): the_post(); ?>
      <?php the_title( '<h1 class="post_title content_title">', '</h1>' ); ?>
      <div class="row post_info">
        <div class="post_meta col">
          <span class="post_meta_date">
            <?php echo get_the_date('M j, Y'); ?> 
          </span>
          <span class="post_meta_dot">
            <i class="fa fa-circle"></i> 
          </span>
          <span class="post_meta_cat">
            <?php $categories = get_the_category(); ?>
            <a href="<?php echo get_category_link($categories[0]->cat_ID); ?>">
            <?php
              if ( ! empty( $categories ) ) {
                  echo esc_html( $categories[0]->name );   
              } else {
                echo "Uncategorized";
              }
            ?>
            </a>
          </span>
        </div>
        <div class="col post_share post_share-top">
          <span class="post_share_label">SHARE: </span><?php get_template_part( 'inc/social', 'share' ); ?>
        </div>
      </div>
      <?php if(get_field('post_thumbnail')) : ?>
      <div class="row">
        <div class="post_thumbnail">
          <img src="<?php echo the_field('post_thumbnail');?>" alt="<?php the_title(); ?>" class="post_thumbnail_img" />
        </div>  
      </div>
      <?php else: ?>
      <div class="row">
        <div class="post_thumbnail">
            <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/assets/img/placeholder.png" />
        </div>  
      </div>
      <?php endif; ?>
      <article class="post_content">
        <?php the_content(); ?>
        <div class="post_share post_share-bottom">
          <span class="post_share_label">SHARE THIS POST:&nbsp; </span> <?php get_template_part( 'inc/social', 'share' ); ?>
        </div>
        <?php if ( is_active_sidebar( 'sidebar-two' ) ) : ?>
          <?php dynamic_sidebar( 'sidebar-two' ); ?>
        <?php endif; ?>
      </article>    
      <?php endwhile; ?> 
      <?php else: ?>
          <p>No content available.</p>
      <?php endif; ?>
    </main>
 </div>
</section>
<?php get_template_part( 'template-parts/related', 'loop' ); ?>
<?php get_footer(); ?>