<section class="search_page page_search">
  <div class="row page_search_row search_page_row">
    <form action="<?php echo home_url( '/' ); ?>" method="GET" id="searchform" class="page_search_form search_page_form" role="search">
      <span class="page_search_form_field search_page_form_field">
        <input type="text" name="s" id="search" class="page_search_form_input search_page_form_input" value="<?php printf( get_search_query() ); ?>" />
        <i class="fa fa-search icon-search page_search_form_icon search_page_form_icon" type="submit"></i>
      </span>
    </form>
  </div>
</section>