<div class="col archive_page_item">          
  <div class="archive_page_item_thumb">
    <a href="<?php the_permalink(); ?>" class="archive_page_item_link">
      <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="archive_page_item_img">
    </a>
  </div>
  <div class="archive_page_item_info">
    <h2 class="archive_page_item_title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <div class="archive_page_item_meta">
      <span class="archive_page_item_date">
        <?php echo get_the_date('M j, Y'); ?> 
      </span>
      <span class="archive_page_item_dot">
        <i class="fa fa-circle"></i> 
      </span>
      <span class="archive_page_item_cat">
        <?php $categories = get_the_category(); ?>
        <a href="<?php echo get_category_link($categories[0]->cat_ID); ?>">
        <?php
          if ( ! empty( $categories ) ) {
              echo esc_html( $categories[0]->name );   
          }
        ?>
        </a>
      </span>
    </div>
    <p class="archive_page_item_excerpt"><?php echo get_the_excerpt(); ?></p>
  </div>
</div>  