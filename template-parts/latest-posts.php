    <?php 
      $the_query = new WP_Query( array(
        'posts_per_page' => 2,
      )); 
    ?>
    <section class="content_latest">
      <div class="row content_latest_row">
        <?php if ( $the_query->have_posts() ) : ?>
        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
        <div class="col content_latest_post">
          <a href="<?php the_permalink(); ?>">
            <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="content_latest_thumbnail">
          </a>
          <div class="content_latest_post_info">
            <a href="<?php the_permalink(); ?>" class="content_latest_post_link">
              <h2 class="content_latest_post_title"><?php the_title(); ?></h2>
            </a>
            <span class="content_latest_post_date">
              <?php echo get_the_date('F j, Y'); ?> 
            </span>
            <span class="content_latest_post_dot">
              <i class="fa fa-circle"></i> 
            </span>
            <span class="content_latest_post_cat">
              <?php $categories = get_the_category(); ?>
              <a href="<?php echo get_category_link($categories[0]->cat_ID); ?>">
                <?php
                  if ( ! empty( $categories ) ) {
                      echo esc_html( $categories[0]->name );   
                  }
                ?>
              </a>
            </span>
          </div>
        </div>
        <?php endwhile; ?>
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
      </div>
    </section>
    