<div class="col content_post_item">          
  <div class="content_post_item_thumb">
    <a href="<?php the_permalink(); ?>" class="content_post_item_link">
      <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="content_post_item_img">
    </a>
  </div>
  <div class="content_post_item_info">
    <h2 class="content_post_item_title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <div class="content_post_item_meta">
      <span class="content_post_item_date">
        <?php echo get_the_date('M j, Y'); ?> 
      </span>
      <span class="content_post_item_dot">
        <i class="fa fa-circle"></i> 
      </span>
      <span class="content_post_item_cat">
        <?php $categories = get_the_category(); ?>
        <a href="<?php echo get_category_link($categories[0]->cat_ID); ?>">
        <?php
          if ( ! empty( $categories ) ) {
              echo esc_html( $categories[0]->name );   
          }
        ?>
        </a>
      </span>
    </div>
    <p class="content_post_item_excerpt"><?php echo get_the_excerpt(); ?></p>
  </div>
</div>  