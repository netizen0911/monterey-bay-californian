<?php 

$category = get_the_category();
$categoryId = $category[0]->cat_ID;
$postId = get_the_ID();

$args = array(
  'post__not_in' => array($postId),
  'post_type' => 'post',
  'post_status' => 'publish',
  'posts_per_page' => '3',
  'category_name' => $category[0]->name,
  'paged' => 1,
);

$related_posts_query = new WP_Query( $args );

if( $related_posts_query->found_posts < 3 ){

  $entries = ($related_posts_query->found_posts < 2) ? 2 : 1;

   $newargs = array(
    'post__not_in' => array($postId),
    'category__not_in' => array($categoryId),
    'post_type' => 'post',
    'post_status' => 'publish',
    'posts_per_page' => $entries,
    'paged' => 1,
   );

   $newquery = new WP_Query( $newargs );

   # merge the two results
  $related_posts_query->posts = array_merge( $related_posts_query->posts, $newquery->posts );
  $related_posts_query->post_count = count( $related_posts_query->posts );
}

?>

<section class="related_post content_post">
  <div class="row  content_post_list">

    <?php if ( $related_posts_query->have_posts() ) : ?>
    <?php while ( $related_posts_query->have_posts() ) : $related_posts_query->the_post(); ?>
    
    <div class="col content_post_item">          
      <div class="content_post_item_thumb">
        <a href="<?php the_permalink(); ?>" class="content_post_item_link">
          <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="content_post_item_img">
        </a>
      </div>
      <div class="content_post_item_info">
        <h2 class="content_post_item_title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        <div class="content_post_item_meta">
          <span class="content_post_item_date">
            <?php echo get_the_date('M j, Y'); ?> 
          </span>
          <span class="content_post_item_dot">
            <i class="fa fa-circle"></i> 
          </span>
          <span class="content_post_item_cat">
            <?php $categories = get_the_category(); ?>
            <a href="<?php echo get_category_link($categories[0]->cat_ID); ?>">
            <?php
              if ( ! empty( $categories ) ) {
                  echo esc_html( $categories[0]->name );   
              }
            ?>
            </a>
          </span>
        </div>
      </div>
    </div>  

    <?php endwhile; ?>     
  </div> <!-- .content_posts_list -->
  <div class="row load_more">
    <div class="col show-more">              
      <?php
        if (  $related_posts_query->max_num_pages > 1 )
          echo '<a href="javascript:void(0)" class="load_more_link loadmore">LOAD MORE</a>'; // you can use <a> as well
      ?>    
    </div> 
    <div class="show-loader" style="display:none">
      <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/assets/img/loader.gif" style="width:100px; height:auto"/>
    </div>
  </div> 
  <?php endif; ?> 
</section>   
<?php wp_reset_postdata(); ?>