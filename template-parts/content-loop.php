    <?php 
      $the_query = new WP_Query( array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'posts_per_page' => '3',
        'offset' => 2,
        'paged' => 1,
      )); 
    ?>
    <section class="content_post">
      <div class="row  content_post_list">

        <?php if ( $the_query->have_posts() ) : ?>
        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
        
        <?php get_template_part( 'template-parts/posts', 'loop' ); ?>

        <?php endwhile; ?>     
      </div> <!-- .content_posts_list -->
      <div class="row load_more">
        <div class="col show-more">              
          <?php
            if (  $the_query->max_num_pages > 1 )
              echo '<a href="javascript:void(0)" class="load_more_link loadmore">LOAD MORE</a>'; // you can use <a> as well
          ?>    
        </div> 
        <div class="show-loader" style="display:none">
          <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/assets/img/loader.gif" style="width:100px; height:auto"/>
        </div>
      </div> 
      <?php endif; ?> 
    </section>   
    <?php wp_reset_postdata(); ?>