//Scroll to top

jQuery(window).scroll(function() {

  var st = jQuery(this).scrollTop();

  if (st > 800) {
      jQuery('#toTop').fadeIn();
  } else {
      jQuery('#toTop').fadeOut();
  }
});

jQuery("#toTop").click(function() {
  jQuery("html, body").animate({scrollTop: 0}, 600);
});
