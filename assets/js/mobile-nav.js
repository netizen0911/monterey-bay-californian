jQuery("#openmenu").click(function(e) {
  e.preventDefault();

  jQuery("#overlay").fadeIn('fast', function(){
    jQuery("#mainmenu").animate({left:'0'});
  });

});

jQuery("#closebtn, #overlay").click(function(e) {
  e.preventDefault();
    jQuery("#mainmenu").animate({left:'-500px'},350);
    jQuery("#overlay").fadeOut();
});


jQuery(".menu-item-has-children > a").click(function(e) {
  e.preventDefault();
  this.classList.toggle("active");

  jQuery(this).next('.sub-menu').slideToggle('fast');
});


// Show on scroll-up

/*
var lastScrollTop = 500;

jQuery(window).scroll(function(){
  var st = jQuery(this).scrollTop();
  var banner = jQuery('.header_top');
  setTimeout(function(){
    if (st > lastScrollTop){
      banner.addClass('hide');
    } else {
      banner.removeClass('hide');
    }
    lastScrollTop = st;
  }, 300);
});

*/

/*
jQuery(document).scroll(function() {
  var y = jQuery(this).scrollTop();
  if (y > 100) {
    jQuery('.header_top').addClass('hide');
  } else {
    jQuery('.header_top').removeClass('hide');
  }
});
*/