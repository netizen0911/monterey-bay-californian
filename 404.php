<?php get_header(); ?>
<section class="content_posts archive_page">
  <div class="row archive_page_heading">
    <div class="col">
      <h2 class="notfound">Oops! Page not found.</h2>
    </div>
  </div>
  <div class="row archive_content">
    <div class="col archive_content_main">
        <div class="col archive_page_list_404" style="padding:20px;">
            <p class="noresults">We apologize. The content that you're looking for is not available.</p>     
        </div>       
    </div> <!-- .archive_content_main -->   

    <div class="col archive_content_sidebar">
      <?php if ( is_active_sidebar( 'sidebar-one' ) ) : ?>
        <?php dynamic_sidebar( 'sidebar-one' ); ?>
      <?php endif; ?>
    </div> <!-- .archive_content_sidebar -->
  </div> <!-- .archive_content -->
</section>    

<?php get_footer(); ?>