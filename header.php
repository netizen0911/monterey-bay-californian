<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php the_title(); ?></title>
    <meta name="description" content="<?php if ( is_single() ) {
      single_post_title('', true); 
      } else {
          bloginfo('name'); echo " - "; bloginfo('description');
      }
      ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo get_bloginfo( 'template_directory' ); ?>/assets/img/favicon.png?v=1.0.1" />
    <link rel="stylesheet" href="<?php echo get_bloginfo( 'template_directory' ); ?>/assets/fonts/fonts.css">
    <link rel="stylesheet" href="<?php echo get_bloginfo( 'template_directory' ); ?>/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo get_bloginfo( 'template_directory' ); ?>/assets/css/style.css?v=<?php echo time(); ?>">

    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
    <!--[if lt IE 7]>
      <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <section class="header_top_nav_mob" id="mainmenu">
      <a href="javascript:void(0);" id="closebtn"><i class="fa fa-times"></i></a>
      <div>
        <a href="<?php echo get_home_url(); ?>">
          <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/assets/img/logo-icon.png" alt="" class="header_top_icon">
        </a>
        <form action="<?php echo home_url( '/' ); ?>" method="GET" id="searchform" class="header_top_search_mob" role="search">
          <span class="header_top_search_mob_field">
            <input type="text" name="s" id="search" class="header_top_search_mob_search" />
            <i class="fa fa-search icon-search"></i>
          </span>
        </form>
        <?php
          wp_nav_menu(
            array(
              'theme_location' => 'primary-menu', 
              'container_class' => 'header_top_nav_container',
              'menu' => 'primary',
              'menu_id' => 'main-menu',
              'menu_class' => 'header_top_nav_mob_list',
            )
          );
        ?>
      </div>
    </section> <!-- header_top_nav_mob -->

    <section class="header_top">
      <div class="row header_top_row">
        <div class="header_top_col header_top_col_left col">
          <a href="javascript:void(0);" id="openmenu" class="header_top_toggle"><i class="fa fa-bars"></i></a>
          <nav class="header_top_nav">
            <div class="header_top_menu">
              <?php
                wp_nav_menu(
                  array(
                    'theme_location' => 'primary-menu', 
                    'container_class' => 'header_top_nav_container',
                    'menu' => 'primary',
                    'menu_id' => 'main-menu',
                    'menu_class' => 'header_top_nav_list',
                  )
                );
              ?>
            </div>
          </nav>
        </div>
        <div class="header_top_col header_top_col_right">
          <div class="header_top_social">
            <span class="header_top_social_label">FOLLOW US: </span>
            <span><a href="" class="header_top_social_link"><i class="fa fa-facebook-f"></i></a></span>
            <span><a href="" class="header_top_social_link"><i class="fa fa-twitter"></i></a></span>
            <span><a href="" class="header_top_social_link"><i class="fa fa-instagram"></i></a></span>
            <span><a href="" class="header_top_social_link"><i class="fa fa-pinterest-p"></i></a></span>
          </div>
        </div>
      </div>
    </section><!-- .header_top -->

    <section class="header_bottom">
      <div class="row header_bottom_row">
        <div class="header_bottom_logo col">
          <a href="<?php echo get_home_url(); ?>" class="header_bottom_logo_link">
            <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/assets/img/logo-top.png" alt="" class="header_bottom_logo_img">
          </a>
        </div>
        <?php if(!is_search()) { ?>
            <div class="col header_bottom_search">
              <form action="<?php echo home_url( '/' ); ?>" method="GET" id="searchform" class="page_search_form" role="search">
                <span class="page_search_form_field">
                  <input type="text" name="s" id="search" class="page_search_form_input" placeholder="SEARCH"/>
                  <i class="fa fa-search page_search_form_icon"></i>
                </span>
              </form>
            </div>
        <?php  }   ?>
      </div>
    </section><!-- .header_bottom -->